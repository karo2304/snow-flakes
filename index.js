const snowFlakesWrapEl = document.querySelector('.snow-flakes-wrap');
const windowWidth = window.innerWidth;
const windowHeight = window.innerHeight;
const flakes = [];
let isWind = false;

const randomNumb = (min, max) => Math.ceil(Math.random() * (max - min) + min);

const randomNumbPosition = (flakeSize, flakeScale) => {
    const scaled = flakeSize * flakeScale;

    return new Promise((resolve, reject) => {
        const randomNumbs = scaled => {
            try {
                const top = randomNumb(0, windowHeight - scaled);
                const left = randomNumb(0, windowWidth - scaled);

                const comparePosition = flakes.find(el => {
                    return (
                        (
                            (left + scaled >= el.left && left + scaled <= el.left + el.width) ||
                            (left >= el.left && left <= el.left + el.width)
                        ) &&
                        (
                            (top >= el.top && top <= el.top + el.height) ||
                            (top + scaled >= el.top && top + scaled <= el.top + el.height)
                        )
                    ) ||
                        (
                            (
                                (el.left + el.width >= left && el.left + el.width <= left + scaled) ||
                                (el.left >= left && el.left <= left + scaled)
                            ) &&
                            (
                                (el.top >= top && el.top <= top + scaled) ||
                                (el.top + el.width >= top && el.top + el.width <= top + scaled)
                            )
                        )
                        ||
                        (
                            ((left + scaled >= el.left && left + scaled <= el.left + el.width) && (left >= el.left && left <= el.left + el.width) && (top >= el.top && top <= el.top + el.height) && (top + scaled >= el.top && top + scaled <= el.top + el.height)) ||
                            ((el.left + el.width >= left && el.left + el.width <= left + scaled) && (el.left >= left && el.left <= left + scaled) && (el.top >= top && el.top <= top + scaled) && (el.top + el.width >= top && el.top + el.width <= top + scaled))
                        )
                });

                !comparePosition || flakes.length === 0 ?
                    resolve({ flakeTop: top, flakeLeft: left, flakeScaled: flakeScale}) :
                    randomNumbs(scaled);
            } catch (err) {
                reject(err);
            }
        };

        randomNumbs(scaled);
    });
};

const flakePosition = (size, randomNumbPosition) => {
    flakes.push({
        width: size * randomNumbPosition.flakeScaled,
        height: size * randomNumbPosition.flakeScaled,
        top: randomNumbPosition.flakeTop,
        left: randomNumbPosition.flakeLeft,
        velocity: randomNumb(2, 6)
    });
};

const createFlakes = async () => {
    const size = 24;

    for (let i = 0; i < 150; i++) {
        const scale = randomNumb(0, 2);
        const getRandomNumbPosition = await randomNumbPosition(size, scale);
        flakePosition(size, getRandomNumbPosition);
    }

    await flakes.forEach(flake => {
        const patternNumb = randomNumb(0, 3);

        snowFlakesWrapEl.innerHTML += `
            <div
                class="snow-flake"
                style="width: ${ flake.width }px;
                        height: ${ flake.height }px;
                        top: ${ flake.top }px;
                        transform: translate(0px, 0px);
                        left: ${ flake.left }px;">
                <div class="snow-flake-wrap flake-pattern-${ patternNumb }">
                    <div class="snow-flake-inner pattern first"></div>
                    <div class="snow-flake-inner pattern second"></div>
                    <div class="snow-flake-inner third"></div>
                    <div class="snow-flake-inner fourth"></div>                    
                </div>
            </div>
        `;
    });

    requestAnimationFrame(snowFlakeAnimation);
};

const snowFlakeAnimation = () => {
    const snowFlakes = document.querySelectorAll('.snow-flakes-wrap .snow-flake');

    snowFlakes.forEach((flake, index) => {
        const [x, y] = flake.style.transform.replaceAll('px', '').match(/[0-9]*\.?[0-9]+(px|%)?/g);
        const currentTranslateX = +x;
        const currentTranslateY = +y;
        const currentTop = +flake.style.top.replace('px','');

        if (currentTranslateY + currentTop + (flakes[index].height * 2) < windowHeight) {

            if (isWind) {
                flake.style.transform = `translate(${currentTranslateX + 1}px, ${currentTranslateY + flakes[index].velocity}px)`;

                setTimeout(() => {
                    isWind = false;
                }, 2000);
            } else {
                flake.style.transform = `translate(${currentTranslateX}px, ${currentTranslateY + flakes[index].velocity}px)`;
            }

            flake.style.transition = 'transform 0.1s';
        } else {
            flake.style.transition = 'unset';
            flake.style.top = `-${flakes[index].height}px`;
            flake.style.transform = `translate(0px,0px)`;
        }
    });

    setTimeout(requestAnimationFrame.bind(null, snowFlakeAnimation), 1000 / 60);
};

const onKeyUp = (event) => {
    if (event.code === 'Space') {
        isWind = true;
    }
};

window.addEventListener('keyup', onKeyUp);

createFlakes();











